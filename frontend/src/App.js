import React, { Component } from 'react';
// import logo from './logo.svg';
import './App.css';
import {Switch,Route,BrowserRouter, Link,Redirect} from 'react-router-dom';
import Home from './views/Home.js';
import Register from './views/Register.js';
import Login from './views/Login.js';
import Ask from './views/Ask.js';
import Message from './views/Message.js';
import Questions from './views/Questions.js';
import QuestionList from './views/QuestionList.js';
import Profile from './views/Profile.js';
import EditQuestion from './views/EditQuestion.js';
import Job from './views/Job.js';
import ShowJob from './views/ShowJob.js';
import EditJob from './views/EditJob.js';
import joblist from './views/JobList.js';
import './scss/custom.scss'; 
import 'bootstrap/dist/js/bootstrap.js';

import { connect } from 'react-redux';
import { logoutUser } from './redux/actions/auth_action';
import { getEmployerId } from './redux/actions/job_actions';
import { saveSocket, addMessageReceiveEvent } from './redux/actions/realtime_action';

// import AuthMiddleware from './middlewares/AuthMiddle';
import { socket } from './config/socketconfig';

/*eslint-disable */
const mapStateToProps = state => {
	return{
		authinfo : state.auth_reducer.authinfo,
		incoming_msg : state.realtime_reducer.incoming_msg,
		incoming_msg_count : state.realtime_reducer.incoming_msg_count,
		// emp : state.job_reducer.edit_jobs
	}
}


const isAuthenticated = (localStorage.getItem('userinfo')==null)? false : true;

const AuthRoute = ({ component: Component, ...rest }) => {
    return (
        <Route {...rest} render={(props) => (
        isAuthenticated === true
            ? <Component {...props} />
            : <Redirect to='/login' />
        )} />
    )
};

/*eslint-disable */
class App extends Component {
	componentDidMount(){
		this.props.saveSocket(socket); // save socket to the store in order to use from everywhere	
		if(this.props.authinfo){
			this.props.addMessageReceiveEvent(socket, this.props.authinfo.user_id);
		}		
		if(this.props.authinfo){
			this.props.getEmployerId(this.props.authinfo.user_id);
		}
	}
	// !important noti need to show for incoming_msg_count
	// componentDidUpdate(prevProps, prevState, snapshot) {		
	// 	if (prevProps.incoming_msg_count !== this.props.incoming_msg_count) {
	// 		alert(this.props.incoming_msg_count);  // need to show noti message count
	// 	}				
	// }
	componentWillUnmount(){
		alert("end");
	}
	render() {
    	return (
			<BrowserRouter>
				<div>
					<nav className="navbar sticky-top navbar-light bg-grey borderbottom navback">
						<div className="container">
							<h4 className="my-0 mr-md-auto font-weight-normal"><Link to='/'>{'{}'} DevPlanet</Link></h4>
							<nav className="my-2 my-md-0 mr-md-3">
								{(this.props.authinfo!=null)? <Link className="p-2 text-dark" to='/ask'>Ask</Link> : ''}
								{(this.props.authinfo!=null)? <Link className="p-2 text-dark" to='/message'>Message</Link> : ''}
								{/* <a className="p-2 text-dark" href="#">Questions</a> */}
								{(this.props.authinfo!=null)? <Link className="p-2 text-dark" to='/question'>Questions</Link> : ''}	
								{(this.props.authinfo!=null)?<Link className="p-2 text-dark" to='/ShowJob'>Jobs</Link>: ''}
								{/* {(this.props.emp.datas!=null)? <Link className="p-2 text-dark" to='/ShowJob'>Jobs</Link> : ''} */}
								<Link className="p-2 text-dark" to='/jobList'>JobList</Link>							
								<a className="p-2 text-dark" href="#">Articles</a>
								<a className="p-2 text-dark" href="#">Advertising</a>
								{(this.props.authinfo!=null)? <Link className="p-2 text-dark" to='/profile'>Profile</Link> : ''}
							</nav>	
							{/* calling stateless component in order to familar with stateless component  */}
							<ShowSignupOrUserinfo logoutUser={this.props.logoutUser} authinfo={this.props.authinfo} />  {/* authinfo={this.props.authinfo} is passing props to stateless */}		
						</div>
					</nav>
					<div className="container">													
						<br/>					
						<Switch>
							<Route exact path="/" component={Home} />
							<Route path="/register" component={Register} /> 
							<Route path="/login" component={Login} />  
							<Route path="/ask" component={Ask} />
							<Route path="/joblist" component={joblist} /> 
							<AuthRoute path="/edit_job/:id" component={EditJob} /> 
							<AuthRoute path="/ShowJob" component={ShowJob} />
							<AuthRoute path="/job" component={Job} />   
							<AuthRoute path="/question" component={Questions} />
							<AuthRoute path="/questionlist" component={QuestionList} />
							<AuthRoute path="/editquestion/:id" component={EditQuestion} />
							<AuthRoute path='/message' component={Message} />
							<AuthRoute path='/profile' component={Profile} />
						</Switch>					
						<br/>					
					</div>
					<footer className="container">
						<p className="float-right"><a href="#">Back to top</a></p>
						<p>© 2017-2018 Company, Inc. · <a href="#">Privacy</a> · <a href="#">Terms</a></p>
					</footer>
				</div>
			</BrowserRouter>
    	);
  	}
}

/*
 * stateless component
 */
const ShowSignupOrUserinfo = (props) => {	
	const logout = (para) => { // writing function with para in stateless comp awesome hah!
		console.log(para); 
		props.logoutUser((message) => {
			alert(message);
			window.location.reload();
		});
	};
	let signuplink = <Link className="btn btn-outline-primary" to='/register'>Sign up</Link>;
	if(props.authinfo != null){
		signuplink = (
			<div className="btn-group btn-sm">
				<button type="button" className="btn btn-danger dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					{props.authinfo.username}
				</button>
				<div className="dropdown-menu">
					{/* onClick={() => logout('a')} passing parameter to stateless function  */}
					<a className="dropdown-item" onClick={() => logout('testing para')} href="#">Logout</a>
				</div>
			</div>
		);
	}						
	return signuplink;
};


export default connect(mapStateToProps, {logoutUser, saveSocket, addMessageReceiveEvent, getEmployerId})(App);
