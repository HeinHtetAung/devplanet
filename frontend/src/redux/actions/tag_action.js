import axioApi from './../../config/axioconfig';

export function getAllTags () {
    return (dispatch) => {
        axioApi.get('/getalltags').then((res) => {
            let tags = res.data
            dispatch({type:'SAVE_ALL_TAGS', tags})
        })
        .catch((err) => {
            dispatch({type:'SAVE_TAGS_ERR', err})
        })
    }
}