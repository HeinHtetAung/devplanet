import axioApi from '../../config/axioconfig';

export function getUsers () {
    return (dispatch) => {
        axioApi.get('users').then((res) => {
            let users = res.data
            dispatch({type:'VIEW_USERS', users})
        })
        .catch((err) => {
            dispatch({type:'VIEW_POSTS_ERR', err})
        })
    }
}

/*
 * Api to save user
 * /users/save 
 * @param { data : {..userinfo} }
 * functiontype : async
 */
export function saveUser(user){
    return async() => {
        let res
        try{
            res = await axioApi.post('users/save', {data : user});
        }catch(e){
            alert("Someting wrong in api calling");
            console.log(e);
            res = e;
        }
        return res;
    }
}

/*
* testing custom promise function
*/
export function testCustomPromiseCheckEven(number){
    return new Promise((resolve, reject) => {
        if(number%2==0){
            resolve({result:true, message:'Yes, it is even number'});
        } 
        reject({result:false, message:'No, it is not even number'});
    });
}