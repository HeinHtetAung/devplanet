import axioApi from './../../config/axioconfig';

export function getQuestionsbyUserId (user_id) {
    return (dispatch) => {
        axioApi.get('/questionsbyuserid/'+user_id).then((res) => {
            let questions = res.data
            dispatch({type:'SAVE_QUESTIONS_BYUSERID', questions})
        })
        .catch((err) => {
            dispatch({type:'SAVE_POSTS_ERR', err})
        })
    }
}

/*
 * Api to save question
 * /questions/save 
 * @param { data : {..questioninfo} }
 */
export function saveQuestion(question){
    return async() => {
        let res
        try{
            res = await axioApi.post('/questions/save', {data : question});
        }catch(e){
            res = e;
        }
        return res;
    }
}

export function getQuestionById (id) {
    return (dispatch) => {
        axioApi.get('/questions/'+id).then((res) => {
            let questions = res.data
            dispatch({type:'SAVE_QUESTIONSBYID', questions})
        })
        .catch((err) => {
            dispatch({type:'SAVE_POSTS_ERR', err})
        })
    }
}

export function delQuestion(id){
    return async() => {
        let res
        try{
            res = await axioApi.get('/questions/delquestion/'+id, {data : id});
        }catch(e){
            res = e;
        }
        return res;
    }
}

export function saveUpdateQuestion(data){
    return async() => {
        let res
        try{
            res = await axioApi.post('/questions/updatequestion/'+data.id, {data : data});
        }catch(e){
            res = e;
        }
        return res;
    }
}