import axioApi from '../../config/axioconfig';

/*
 * Api for login
 * /users/login
 * @param { email : ..., password : ... }
 * functiontype : callback
 */
export function loginUser(data, callback){
    return (dispatch) => {
        axioApi.post('auth/login', data).then((response) => {
            if(response.data.result === true){
                const authinfo = {token : response.data.token, user_id : response.data.user_id, username : response.data.username};
                dispatch({type:'SAVE_USER_INFO', authinfo});
                localStorage.setItem('userinfo', JSON.stringify(authinfo));
                axioApi.defaults.headers.common['x-access-token'] = authinfo.token;
            }
            callback(response.data);                        
        }).catch((error) => {
            alert("something wrong in api calling 1s");
            console.log(error);  
        });
    }
}

/*
 * Logout user
 * maybe api calling stuff in future like updating logout time 
 * function type : callback
 */
export function logoutUser(callback){
    return (dispatch) => {
        localStorage.removeItem('userinfo');
        const authinfo = null;
        dispatch({type:'SAVE_USER_INFO', authinfo});
        callback('Logout success'); // maybe api response need in future
    }
}


/*
 * Api for login
 * /users/login
 * @param { email : ..., password : ... }
 * functiontype : promise
 */
export function loginUserPromise(data, callback){
    return axioApi.post('users/login', data);
}

/*
 * Check token in every component update
 */
export function checkToken(){
    return (dispatch) => {
        axioApi.get('auth/user').then((res) => { 
            // console.log(res);              
        }).catch((err) => {
                        
        });
    }
}