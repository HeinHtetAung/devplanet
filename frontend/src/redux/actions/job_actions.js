import axioApi from './../../config/axioconfig';

export function getJobs () {
    return (dispatch) => {
        axioApi.get('jobs').then((res) => {
            let jobs = res.data

            dispatch({type:'VIEW_JOBS', jobs})
        })
        .catch((err) => {
            dispatch({type:'VIEW_POSTS_ERR', err})
        })
    }
}

/*
 * Api to save user
 * /jobs/save 
 * @param { data : {..jobinfo} }
 */
export function saveJob(job){
   return async() => {
       let res
       try{
           res = await axioApi.post('/jobs/save', {data : job});
       }catch(e){
           res = e;
       }
       return res;
   }
}

export function deleteJob(id){
   return async() => {
       let res
       try{
        res = await axioApi.get('/delete_jobs/'+id, {data : id});
          // console.log("res" + res);
       }catch(e){
           res = e;
       }
       return res;
   }
}

export function getJobsById(id){
    return (dispatch) => {
        axioApi.get('jobs/'+id).then((res) => {
            let jobs = res.data
            // console.log(jobs);
            dispatch({type:'EDIT_JOBS', jobs})
        })
        .catch((err) => {
            dispatch({type:'EDIT_POSTS_ERR', err})
        })
    }
}

export function saveUpdateJob(data){
  console.log("data");
    return async() => {
        let res
        try{
            res = await axioApi.post('/updatejob/'+data.id, {data : data});
        }catch(e){
            res = e;
        }
        return res;
    }
}

export function getEmployerId(user_id){
    return (dispatch) => {
        axioApi.get('jobs/emp/'+user_id).then((res) => {
            let jobs = res.data
            console.log(jobs);
            dispatch({type:'EDIT_JOBS', jobs})
        })
        .catch((err) => {
            dispatch({type:'EDIT_POSTS_ERR', err})
        })
    }
}

export function getJobsByUserId(user_id){
    return (dispatch) => {
        axioApi.get('jobs/user/'+user_id).then((res) => {
            let jobs = res.data
            // console.log(jobs);
            dispatch({type:'VIEW_JOBS', jobs})
        })
        .catch((err) => {
            dispatch({type:'VIEW_POSTS_ERR', err})
        })
    }
}

