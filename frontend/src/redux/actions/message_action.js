import axioApi from '../../config/axioconfig';

/*
 * Api to save message
 * /message/save 
 * @param { data : {sent_user_id : .., receive_user_id : .., msgtext : .., file_type : .., file_url : ..} }
 * functiontype : async
 */
export function saveMessage(message){
    return async() => {
        let res
        try{
            res = await axioApi.post('message/save', {data : message});
            res = res.data;
        }catch(e){
            alert("Someting wrong in api calling");
            console.log(e);
            res = e;
        }
        return res;
    }
}

export function getChatUsers (user_id) {
    return (dispatch) => {
        axioApi.get('message/chatusers/'+user_id).then((res) => {
            let chat_users = res.data
            dispatch({type:'SAVE_CHAT_USERS', chat_users})
        })
        .catch((err) => {
            console.log(err);
            // dispatch({type:'VIEW_POSTS_ERR', err})
        })
    }
}

export function getMessage(user1, user2) {
    return (dispatch) => {
        axioApi.get('message/getmessage/'+user1+'/'+user2).then((res) => {
            let messages = res.data
            dispatch({type:'SAVE_MESSAGES', messages})
        })
        .catch((err) => {
            console.log(err);
            // dispatch({type:'VIEW_POSTS_ERR', err})
        })
    }
}

export function updateChatUser(arr){
    return (dispatch) => {
        dispatch({type:'SAVE_CHAT_USERS', arr});
    }
}

export function updateAllUser(arr){
    return (dispatch) => {
        dispatch({type:'VIEW_USERS', arr})
    }
}


/*
 * Api to change read message
 * /message/read 
 * @param POST { data : {sent_user_id : .., receive_user_id : ..} }
 * functiontype : async
 */
export function setReadAll(receive_user_id, sent_user_id, callback){
    return (dispatch) => {
        const data = {receive_user_id : receive_user_id, sent_user_id : sent_user_id};
        axioApi.put('message/read', {data : data}).then((res) => {
            callback(res);
        }).catch((e) => {
            alert("Something wrong");
            console.log(e);
        });
    }
}