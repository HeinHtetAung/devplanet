export function saveSocket(socket){
    return (dispatch) => {
        dispatch({type:'SAVE_SOCKET', socket});
    }
}

export function addMessageReceiveEvent(socket, user_id){
    return (dispatch) => {
        socket.on('message_receive', (e) => {
            console.log('********** message received *************');
            console.log(e);
            console.log(user_id);
            if(e.socket_send_to === user_id){
                const message = e.message;
                dispatch({type:'INCOMING_MSG', message});
                dispatch({type:'INCOMING_MSG_CNT', message});
            }
        });
    }
}

export function socketEmitMessage(socket, user_id, data){
    return (dispatch) => {
        socket.emit('message_send',{'message' : data, 'socket_send_to' : user_id}); 
    }
}