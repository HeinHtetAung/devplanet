const initialState = {
    questions : [],
    questionsbyid : []
}
export default (state=initialState, action) => {
    switch (action.type) {
        case 'SAVE_QUESTIONS_BYUSERID' :
            return {
                ...state,
                questions: action.questions
            }
            
        case 'SAVE_QUESTIONSBYID' :
            return {
                ...state,
                questionsbyid: action.questions
            }
        default:
            return state
    }
} 
