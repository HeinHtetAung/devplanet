const initialState = {
    socket : null,
    incoming_msg : null,
    incoming_msg_count : 0
}
export default (state=initialState, action) => {
    switch (action.type) {
        case 'SAVE_SOCKET' :
            return {
                ...state,
                socket: action.socket
            }
        case 'INCOMING_MSG' :
            return {
                ...state,
                incoming_msg : action.message
            }
        case 'INCOMING_MSG_CNT' :
            return {
                ...state,
                incoming_msg_count : state.incoming_msg_count + 1
            }
        default:
            return state
    }
} 
