const initialState = {
    tags : []
}
export default (state=initialState, action) => {
    switch (action.type) {
        case 'SAVE_ALL_TAGS' :
            return {
                ...state,
                tags: action.tags
            }
        
        default:
            return state
    }
} 
