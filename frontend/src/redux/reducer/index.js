import { combineReducers } from 'redux';
import user_reducer from './user_reducer';
import auth_reducer from './auth_reducer';
import message_reducer from './message_reducer';
import realtime_reducer from './realtime_reducer';
import question_reducer from './question_reducer'
import tag_reducer from './tag_reducer';
import job_reducer from './job_reducer';
import { routerReducer } from 'react-router-redux';
 export default combineReducers({
  user_reducer,
  auth_reducer,
  message_reducer,
  router: routerReducer,
  realtime_reducer,
  question_reducer,
  tag_reducer,
  job_reducer,
});