const initialState = {
    jobs : [],
    edit_jobs :[]
}
export default (state=initialState, action) => {
    switch (action.type) {
        case 'VIEW_JOBS' :
            return {
                ...state,
                jobs: action.jobs
            }
        case 'EDIT_JOBS' :
            return {
                ...state,
                edit_jobs: action.jobs
            }
        default:
            return state
    }
} 
