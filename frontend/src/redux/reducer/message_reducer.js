const initialState = {
    chat_users : [],
    messages : []
}
export default (state=initialState, action) => {
    switch (action.type) {
        case 'SAVE_CHAT_USERS' :
            return {
                ...state,
                chat_users: action.chat_users
            }
        case 'SAVE_MESSAGES' :
            return {
                ...state,
                messages: action.messages
            }
        default:
            return state
    }
} 
