import React, { Component } from 'react';
import { getJobs,deleteJob,getEmployerId,getJobsByUserId } from './../redux/actions/job_actions';
import {Switch,Route,BrowserRouter, Link} from 'react-router-dom';
import { checkToken } from '../redux/actions/auth_action';
import { connect } from 'react-redux';

const mapStateToProps = state => {
    return{
        jobs : state.job_reducer.jobs,
        authinfo : state.auth_reducer.authinfo,
        emp : state.job_reducer.edit_jobs
    }
}

class ShowJobs extends Component {
    componentDidMount(){
        const user_id = this.props.authinfo.user_id;
        this.props.getEmployerId(user_id);
        this.props.getJobsByUserId(user_id);
    }
    async deleteJob(id){
        var res = window.confirm("Are you sure want to delete?");
        if(res == true){
            const jobid = id;
            const returndata = await this.props.deleteJob(jobid);
            if(!returndata.data){
                alert("Api error");
                return false;
            }
            if(returndata.data.result == true){
                alert("Deleted Job Successfully!!");
                window.location.reload();
            }else if(returndata.data.result == false){
                alert("validation error");
            }else{
                alert("something error");
            }
        }
    }

    link(){
        if(this.props.emp.datas){
            return <Link className="btn btn-primary" to={'/job/'}>Create Job</Link>;
        }
    }
 
    tabRows(){
        if(this.props.jobs.datas){
            return this.props.jobs.datas.map((d, i) => {
                // return <li key={i}><Link to={'/delete_advertiser/'+d.id}>{d.advertiser_name}</Link></li>;
                return <tr key={i}>
                        <td>{d.emp.company_name}</td>
                        <td>{d.job_title}</td>
                        <td>{d.job_description}</td>
                        <td>{d.salary}</td>
                        <td><Link to={'/edit_job/'+d.id} className="btn btn-success">Edit</Link></td>
                        <td><button className="btn btn-danger" onClick={() => this.deleteJob(d.id)} >Delete</button></td>
                    </tr>
            });
        }
    }
    render() {
        return (
            <div>
                ShowJobs layout!...
                {this.link()}
                <br/><br/>
                <table className="table table-hover">
                    <thead>
                        <tr>
                            <th>Company Name</th>
                            <th>Job Title</th>
                            <th>Job Description</th>
                            <th>Salary</th>
                            <th colSpan="2">Options</th>
                        </tr>
                    </thead>

                    <tbody>
                        {this.tabRows()}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default connect(mapStateToProps, { getJobs,deleteJob,checkToken,getEmployerId,getJobsByUserId })(ShowJobs);