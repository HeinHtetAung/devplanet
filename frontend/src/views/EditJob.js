import React, { Component } from "react";
import { getJobsById, saveUpdateJob } from "./../redux/actions/job_actions";
import { getAllTags } from "./../redux/actions/tag_action";
import Creatable, { useCreatable } from "react-select/creatable";
import { Switch, Route, BrowserRouter, Link } from "react-router-dom";
import { connect } from "react-redux";

const mapStateToProps = (state) => {
  return {
    jobs: state.job_reducer.edit_jobs,
    tags: state.tag_reducer.tags.datas,
  };
};

class Job extends Component {
  constructor(props) {
    super(props);
    this.state = { tagSelect: "" };
  }
  componentDidMount() {
    const id = this.props.match.params.id;
    this.props.getJobsById(id);
    this.props.getAllTags();
  }
  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.jobs !== this.props.jobs) {
      console.log(this.props.jobs.datas);
      document.getElementById(
        "employer_id"
      ).value = this.props.jobs.datas.employer_id;
      document.getElementById(
        "job_title"
      ).value = this.props.jobs.datas.job_title;
      document.getElementById(
        "job_description"
      ).value = this.props.jobs.datas.job_description;
      document.getElementById("salary").value = this.props.jobs.datas.salary;
      this.setState({ tagSelect: this.props.jobs.tag_names });
    }
  }
  handleChange = (newValue: any, actionMeta: any) => {
    this.setState({ tagSelect: newValue });
  };
  async updateJob(e) {
    e.preventDefault();
    const arr = {
      id: this.props.match.params.id,
      employer_id: document.getElementById("employer_id").value,
      job_title: document.getElementById("job_title").value,
      job_description: document.getElementById("job_description").value,
      tags: this.state.tagSelect,
      salary: document.getElementById("salary").value,
    };
    const returndata = await this.props.saveUpdateJob(arr);
    if (!returndata.data) {
      alert("Api error");
      // console.log(returndata);
      return false;
    }
    if (returndata.data.result == true) {
      alert("Successfully Save Updatejob!");
      this.props.history.push("/ShowJob");
    } else if (returndata.data.result == false) {
      alert("validation error");
      console.log(returndata.data);
    } else {
      alert("something error");
      console.log(returndata);
    }
  }

  render() {
    let alltagval;
    let alltagvalarr = Array();
    if (this.props.tags) {
      this.props.tags.map((d, i) => {
        var label = d.name;
        var id = d.id;
        alltagval = { label: label, value: id };
        alltagvalarr.push(alltagval);
      });
    }
    return (
      <div className="container">
        <form onSubmit={(e) => this.updateJob(e)}>
          <div className="form-group">
            <input
              hidden
              type="text"
              className="form-control"
              id="employer_id"
              aria-describedby="emailHelp"
            />
          </div>
          <div className="form-group">
            <label htmlFor="exampleInputEmail1">Job Title</label>
            <input
              type="text"
              className="form-control"
              id="job_title"
              aria-describedby="emailHelp"
            />
          </div>
          <div className="form-group">
            <label htmlFor="exampleInputEmail1">Job Description</label>
            <input
              type="text"
              className="form-control"
              id="job_description"
              aria-describedby="emailHelp"
              placeholder="Enter Job Description"
            />
          </div>
          <div className="form-group">
            <label htmlFor="exampleInputPassword1">Salary</label>
            <input
              type="text"
              className="form-control"
              id="salary"
              aria-describedby="emailHelp"
              placeholder="Salary"
            />
          </div>
          <div className="form-group">
            <label htmlFor="tags" style={{ color: "#007BFF" }}>
              Tags
            </label>
            <Creatable
              isMulti
              value={this.state.tagSelect}
              onChange={this.handleChange}
              options={alltagvalarr}
              id="tags"
            />
          </div>
          {/* <div className="form-check">
                    <input type="checkbox" className="form-check-input" id="exampleCheck1" />
                    <label className="form-check-label" htmlFor="exampleCheck1">Check me out</label>
               </div> */}
          <button type="submit" className="btn btn-primary">
            Submit
          </button>
        </form>
      </div>
    );
  }
}

export default connect(mapStateToProps, {
  getJobsById,
  saveUpdateJob,
  getAllTags,
})(Job);
