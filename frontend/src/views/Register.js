import React, { Component } from 'react';
import { saveUser } from '../redux/actions/user_action';
import { connect } from 'react-redux';
import {Link} from 'react-router-dom';
import $ from 'jquery';

/*eslint-disable */
const mapStateToProps = state => {
  return{
      // users : state.user_reducer.users  // redux_step4 getting data from store and connect with view
  }
}

class Register extends Component {
    componentDidMount(){
        $('#forEmp').css('display','none');
    }
    onSiteChanged(e) {
        const rd_developer = document.getElementById('rd_developer').checked;
        const rd_employer = document.getElementById('rd_employer').checked; 
        if(rd_employer){
            $('#forDev').css('display','none');
            $('#forEmp').css('display','block');
        }
        if(rd_developer){
            $('#forEmp').css('display','none');
            $('#forDev').css('display','block');
        }      
    }

    async register(e){
        e.preventDefault();
        const rd_developer = document.getElementById('rd_developer').checked;
        const rd_employer = document.getElementById('rd_employer').checked;    
        const rd_open = document.getElementById('rd_open').checked;    
        const rd_open_no = document.getElementById('rd_open_no').checked;    
        const arr = {
            username : document.getElementById('username').value,
            email : document.getElementById('email').value,
            password : document.getElementById('password').value,
            current_position : document.getElementById('txt_current_position').value,
            skill : document.getElementById('txt_skill').value,
            available_time : document.getElementById('txt_available_time').value,
            person_name : document.getElementById('person_name').value,
            company_name : document.getElementById('company_name').value,
            company_info : document.getElementById('company_info').value,
            open_close : (rd_open==true)? 1 : 0,
            is_developer : (rd_developer==true)? 1 : 0,
            is_employer : (rd_employer==true)? 1 : 0
        };    
        const returndata = await this.props.saveUser(arr);
        if(!returndata.data){
            alert("Api error"); 
            console.log(returndata); 
            return false;
        }
        if(returndata.data.result == true){
            alert("Registered! please login");
            this.props.history.push('/login')
        }else if(returndata.data.result == false){
            console.log(returndata.data);
            alert("validation error \n" + JSON.stringify(returndata.data));
        }else{
            alert("something error"); 
            console.log(returndata);
        }
    }
  render() {
    return (
        <div className="container">
            <form onSubmit={(e) => this.register(e)}>
                <div className="form-group">
                    <label >Username</label>
                    <input type="text" className="form-control" id="username" aria-describedby="emailHelp" placeholder="Enter Full Name" />
                </div>
                <div className="form-group">
                    <label >Email address</label>
                    <input type="email" className="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email" />
                    {/* <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small> */}
                </div>
                <div className="form-group">
                    <label >Password</label>
                    <input type="password" className="form-control" id="password" placeholder="Password" />
                </div>
                <div className="form-group">
                    <label>Who are you ?</label>
                    <br />
                    <input defaultChecked type="radio" id="rd_developer" name="whoareyou" onClick={(e) => this.onSiteChanged()} /> Developer &nbsp;&nbsp; <input type="radio" id="rd_employer" name="whoareyou" onClick={(e) => this.onSiteChanged()}/> Employer 
                </div>

                <div id="forDev">
                    <div className="form-group">
                        <label >Current Position</label>
                        <input type="text" className="form-control" id="txt_current_position" aria-describedby="emailHelp" placeholder="Senior Engineer at Facebook" />
                    </div>          
                    <div className="form-group">
                        <label >Skill</label>
                        <input type="text" className="form-control" id="txt_skill" aria-describedby="emailHelp" placeholder="Enter Full Name" />
                    </div>
                    <div className="form-group">
                        <label>Would you open to answer if someone ask you question or get help related with your technology stack.</label>
                        <br />
                        <input defaultChecked type="radio" id="rd_open" name="open" /> Yes &nbsp;&nbsp; <input type="radio" id="rd_open_no" name="open" /> No
                    </div>
                    <div className="form-group">
                        <label >Available Time</label>
                        <input type="text" className="form-control" id="txt_available_time" aria-describedby="emailHelp" placeholder="Time Range Text box Later" />
                    </div>
                </div>

                <div id="forEmp">
                    <div className="form-group">
                        <label >Preson Name</label>
                        <input type="text" className="form-control" id="person_name" aria-describedby="emailHelp" placeholder="Please Enter Person Name" />
                    </div>          
                    <div className="form-group">
                        <label >Company Name</label>
                        <input type="text" className="form-control" id="company_name" aria-describedby="emailHelp" placeholder="Enter Company Name" />
                    </div>
                    <div className="form-group">
                        <label >Company Info</label>
                        <input type="text" className="form-control" id="company_info" aria-describedby="emailHelp" placeholder="Enter Company Info" />
                    </div>
                </div>

                {/* <div className="form-check">
                    <input type="checkbox" className="form-check-input" id="exampleCheck1" />
                    <label className="form-check-label" for="exampleCheck1">Check me out</label>
                </div> */}
                <button type="submit" className="btn btn-primary">Submit</button>
            </form>
            <br/>
            Already have and account? <Link to='/login'>Please Login here</Link>.
            <br/>
        </div>      
    );
  }
}

export default connect(mapStateToProps, { saveUser })(Register);
