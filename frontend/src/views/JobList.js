import React, { Component } from 'react';
import { getJobs,deleteJob,getEmployerId } from './../redux/actions/job_actions';
import {Switch,Route,BrowserRouter, Link} from 'react-router-dom';
import { checkToken } from '../redux/actions/auth_action';
import { connect } from 'react-redux';

const mapStateToProps = state => {
    return{
        jobs : state.job_reducer.jobs,
    }
}

class ShowJobs extends Component {
    componentDidMount(){
        this.props.getJobs();
    }
    async deleteJob(id){
        var res = window.confirm("Are you sure want to delete?");
            if(res == true){
                const jobid = id;
                const returndata = await this.props.deleteJob(jobid);
                console.log(returndata);
            if(!returndata.data){
                alert("Api error");
                console.log(returndata);
                return false;
            }
            if(returndata.data.result == true){
                alert("Deleted Job Successfully!!");
                window.location.reload();
            }else if(returndata.data.result == false){
                alert("validation error");
                console.log(returndata.data);
            }else{
                alert("something error");
                console.log(returndata);
            }
        }
    }

    tabRows(){
        if(this.props.jobs.datas){
            return this.props.jobs.datas.map((d, i) => {
                var date = d.createdAt,
                cdate = (new Date(date)).toLocaleDateString('en-US');
                // return <li key={i}><Link to={'/delete_advertiser/'+d.id}>{d.advertiser_name}</Link></li>;
                return <div><div style={{ border: '1px solid gray' , borderRadius:'4px', width:'900px'}}> 
                            <span style={{ marginLeft: '20px',fontSize:'22px',color:'#007BFE'}}>{d.job_title}</span>
                            <span style={{ marginRight: '20px' ,float: 'right',fontSize:'14px'}}>{cdate}</span>
                            <span style={{ marginRight: '5px' ,float: 'right',fontSize:'15px',color:'#007BFE'}}>posted:</span><br />
                            <span style={{ marginLeft: '20px',fontSize:'14px'}}>{d.emp.company_name}</span><br />
                            <span style={{ marginLeft: '20px',fontSize:'14px'}}>{d.salary}</span><br />
                            <span style={{ marginLeft: '20px',fontSize:'13px'}}>{d.job_description}</span><br />
                        </div><br/></div>
            });
        }
    }
    render() {
        return (
            <div>
                ShowJobs layout!...
                <br/><br/>
                {this.tabRows()}
            </div>
        );
    }
}

export default connect(mapStateToProps, { getJobs,deleteJob,checkToken,getEmployerId })(ShowJobs);