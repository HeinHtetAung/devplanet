import React, { Component } from "react";
import { connect } from "react-redux";
import {
  getQuestionById,
  saveUpdateQuestion,
} from "./../redux/actions/question_action";
import { getAllTags } from "./../redux/actions/tag_action";
import Creatable, { useCreatable } from "react-select/creatable";
import { Link } from "react-router-dom";

const mapStateToProps = (state) => {
  return {
    questions: state.question_reducer.questionsbyid, // redux_step4 getting data from store and connect with view
    tags: state.tag_reducer.tags.datas,
  };
};

class EditQuestion extends Component {
  constructor(props) {
    super(props);
    this.state = { tagSelect: "" };
  }
  componentDidMount() {
    this.props.getQuestionById(this.props.match.params.id);
    this.props.getAllTags();
  }
  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.questions !== this.props.questions) {
      // if question come
      document.getElementById(
        "title"
      ).value = this.props.questions.datas[0].title;
      document.getElementById(
        "description"
      ).value = this.props.questions.datas[0].description;
      this.setState({ tagSelect: this.props.questions.tag_names });
    }
  }
  handleChange = (newValue: any, actionMeta: any) => {
    this.setState({ tagSelect: newValue });
  };
  async updatequestion(e) {
    e.preventDefault();
    const arr = {
      id: this.props.match.params.id,
      title: document.getElementById("title").value,
      description: document.getElementById("description").value,
      tags: this.state.tagSelect,
    };
    const returndata = await this.props.saveUpdateQuestion(arr);
    if (!returndata.data) {
      alert("Api error");
      console.log(returndata);
      return false;
    }
    if (returndata.data.result == true) {
      alert("Successfully Save UpdateQuestion!");
      this.props.history.push("/questionlist");
    } else if (returndata.data.result == false) {
      alert("validation error");
      console.log(returndata.data);
    } else {
      alert("something error");
      console.log(returndata);
    }
  }
  render() {
    let alltagval;
    let alltagvalarr = Array();
    if (this.props.tags) {
      this.props.tags.map((d, i) => {
        var label = d.name;
        var id = d.id;
        alltagval = { label: label, value: id };
        alltagvalarr.push(alltagval);
      });
    }
    return (
      <div className="container">
        <form onSubmit={(e) => this.updatequestion(e)}>
          <div>
            <div className="form-group">
              <label htmlFor="title" className="header">
                Title
              </label>
              <input
                type="text"
                className="form-control"
                id="title"
                aria-describedby="emailHelp"
              />
            </div>
            <div className="form-group">
              <label htmlFor="description" className="header">
                Description
              </label>
              <textarea
                className="form-control"
                id="description"
                aria-describedby="emailHelp"
              />
            </div>
          </div>
          <div className="form-group">
            <label htmlFor="tags" style={{ color: "#007BFF" }}>
              Tags
            </label>
            <Creatable
              isMulti
              value={this.state.tagSelect}
              onChange={this.handleChange}
              options={alltagvalarr}
              id="tags"
            />
          </div>
          <button type="submit" className="btn btn-primary">
            Update
          </button>
          &nbsp;
          <button
            type="button"
            className="btn"
            onClick={this.props.history.goBack}
            style={{ color: "#007BFF" }}
          >
            Cancel
          </button>
        </form>
      </div>
    );
  }
}

export default connect(mapStateToProps, {
  getQuestionById,
  saveUpdateQuestion,
  getAllTags,
})(EditQuestion);
