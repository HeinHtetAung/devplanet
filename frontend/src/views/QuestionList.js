import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getQuestionsbyUserId,delQuestion } from '../redux/actions/question_action';
import {Link} from 'react-router-dom';

const mapStateToProps = state => {
    return{
        questions : state.question_reducer.questions,  // redux_step4 getting data from store and connect with view
        authinfo : state.auth_reducer.authinfo
    }
}

class QuestionList extends Component {
    componentDidMount(){
        this.props.getQuestionsbyUserId(this.props.authinfo.user_id);
    }
    async deleteQuestion(id){
        var res = window.confirm("Are you sure want to delete?");
        if(res == true){
            const returndata = await this.props.delQuestion(id);
            if(!returndata.data){
                alert("Api error"); 
                console.log(returndata); 
                return false;
            }
            if(returndata.data.result == true){
                alert("Successfully deleted!");
                window.location.reload(); 
            }else if(returndata.data.result == false){
                console.log(returndata.data);
                alert("validation error \n" + JSON.stringify(returndata.data));
            }else{
                alert("something error"); 
                console.log(returndata);
            }
        }
    }
    tabRows(){    
        if(this.props.questions.datas){
            return this.props.questions.datas.map((d, i) => {
                return <tr key={i}>
                <td>{i+1}</td>
                <td>{d.title}</td>
                <td>{d.description}</td>
                <td><Link to={'/editQuestion/'+d.id} style={{ textDecoration: 'none' }}><button className="btn" style={{ color: "#007BFF" }}>Edit</button></Link>
                &nbsp;<button onClick={() => this.deleteQuestion(d.id)} className="btn btn-primary">Delete</button></td>
                </tr>;
            });              
        }    
    }
    render() {
        return (
            <div>
                <div>
                    <h5 className="header">QuestionList</h5>
                    <table className='table'>
                        <thead>
                        <tr>
                            <th className="header">No.</th>
                            <th className="header">Title</th>
                            <th className="header">Description</th>
                            <th className="header">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                            {this.tabRows()}
                        </tbody>
                    </table>
                    <Link to={'/question'} style={{ textDecoration: 'none' }}><button type="button" className="btn" style={{ color: "#007BFF" }}>Ask Question</button></Link>
                    &nbsp;<button type="button" className="btn btn-primary" onClick={this.props.history.goBack}>Cancel</button>
                </div>
            <div className="container"> 
            </div>
            </div>
        );
    }
}

export default connect(mapStateToProps, { getQuestionsbyUserId,delQuestion })(QuestionList);