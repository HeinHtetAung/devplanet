import React, { Component } from "react";
import { saveQuestion } from "./../redux/actions/question_action";
import { getAllTags } from "./../redux/actions/tag_action";
import Creatable, { useCreatable } from "react-select/creatable";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

const mapStateToProps = (state) => {
  return {
    authinfo: state.auth_reducer.authinfo, // redux_step4 getting data from store and connect with view
    tags: state.tag_reducer.tags.datas,
  };
};

class Question extends Component {
  componentDidMount() {
    this.props.getAllTags();
  }
  selectedMsgTemplate1 = "";
  handleChange = (newValue: any, actionMeta: any) => {
    this.selectedMsgTemplate1 = newValue;
  };
  async savequestion(e) {
    e.preventDefault();
    const arr = {
      title: document.getElementById("title").value,
      description: document.getElementById("description").value,
      tags: this.selectedMsgTemplate1,
      user_id: this.props.authinfo.user_id,
    };
    const returndata = await this.props.saveQuestion(arr);
    if (!returndata.data) {
      alert("Api error");
      console.log(returndata);
      return false;
    }
    if (returndata.data.result == true) {
      alert("Successfully SaveQuestion!");
      this.props.history.push("/questionlist");
    } else if (returndata.data.result == false) {
      alert("validation error \n" + JSON.stringify(returndata.data));
      console.log(returndata.data);
    } else {
      alert("something error");
      console.log(returndata);
    }
  }
  render() {
    let alltagval;
    let alltagvalarr = Array();
    if (this.props.tags) {
      this.props.tags.map((d, i) => {
        var label = d.name;
        var id = d.id;
        alltagval = { label: label, value: id };
        alltagvalarr.push(alltagval);
      });
    }
    return (
      <div className="container">
        <form onSubmit={(e) => this.savequestion(e)}>
          <div className="form-group">
            <label htmlFor="title" style={{ color: "#007BFF" }}>
              Title
            </label>
            <input
              type="text"
              className="form-control"
              id="title"
              aria-describedby="emailHelp"
              placeholder="Enter Title"
            />
          </div>
          <div className="form-group">
            <label htmlFor="description" style={{ color: "#007BFF" }}>
              Description
            </label>
            <textarea
              className="form-control"
              id="description"
              aria-describedby="emailHelp"
              placeholder="Enter description"
            />
          </div>
          <div className="form-group">
            <label htmlFor="tags" style={{ color: "#007BFF" }}>
              Tags
            </label>
            <Creatable
              isMulti
              onChange={this.handleChange}
              options={alltagvalarr}
              id="tags"
            />
          </div>
          <button type="submit" className="btn btn-primary">
            Submit
          </button>
          &nbsp;
          <button
            type="button"
            className="btn btn-primary"
            onClick={this.props.history.goBack}
          >
            Cancel
          </button>
          <Link to={"/questionlist"} style={{ textDecoration: "none" }}>
            <button
              type="button"
              className="btn"
              style={{ marginLeft: "755px", color: "#007BFF" }}
            >
              View QuestionList
            </button>
          </Link>
        </form>
      </div>
    );
  }
}

export default connect(mapStateToProps, { saveQuestion, getAllTags })(Question);
