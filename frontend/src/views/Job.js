import React, { Component } from "react";
import { saveJob, getEmployerId } from "./../redux/actions/job_actions";
import { checkToken } from "../redux/actions/auth_action";
import { getAllTags } from "./../redux/actions/tag_action";
import Creatable, { useCreatable } from "react-select/creatable";
import { connect } from "react-redux";
/*eslint-disable */
const mapStateToProps = (state) => {
  return {
    authinfo: state.auth_reducer.authinfo,
    emp: state.job_reducer.edit_jobs,
    tags: state.tag_reducer.tags.datas,
  };
};

class Job extends Component {
  componentDidMount() {
    const user_id = this.props.authinfo.user_id;
    this.props.getEmployerId(user_id);
    this.props.getAllTags();
  }
  selectedMsgTemplate1 = "";
  handleChange = (newValue: any, actionMeta: any) => {
    this.selectedMsgTemplate1 = newValue;
  };
  async job(e) {
    e.preventDefault();
    const arr = {
      employer_id: this.props.emp.datas.id,
      job_description: document.getElementById("job_description").value,
      salary: document.getElementById("salary").value,
      job_title: document.getElementById("job_title").value,
      tags: this.selectedMsgTemplate1,
      user_id: this.props.authinfo.user_id,
    };
    console.log(arr);
    const returndata = await this.props.saveJob(arr);
    console.log("returndata " + returndata);

    if (!returndata.data) {
      alert("Api error");
      console.log(returndata);
      return false;
    }
    if (returndata.data.result == true) {
      alert("Job portal success!");
      this.props.history.push("/ShowJob");
    } else if (returndata.data.result == false) {
      alert("validation error");
      console.log(returndata.data);
    } else {
      alert("something error");
      console.log(returndata);
    }
  }
  render() {
    let alltagval;
    let alltagvalarr = Array();
    if (this.props.tags) {
      this.props.tags.map((d, i) => {
        var label = d.name;
        var id = d.id;
        alltagval = { label: label, value: id };
        alltagvalarr.push(alltagval);
      });
    }
    return (
      <div className="container">
        <form onSubmit={(e) => this.job(e)}>
          <div className="form-group">
            <label htmlFor="exampleInputEmail1">Job Title</label>
            <input
              type="text"
              className="form-control"
              id="job_title"
              aria-describedby="emailHelp"
              placeholder="Enter Job Title"
            />
          </div>
          <div className="form-group">
            <label htmlFor="exampleInputEmail1">Job Description</label>
            <input
              type="text"
              className="form-control"
              id="job_description"
              aria-describedby="emailHelp"
              placeholder="Enter Job Description"
            />
            {/* <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small> */}
          </div>
          <div className="form-group">
            <label htmlFor="exampleInputPassword1">Salary</label>
            <input
              type="text"
              className="form-control"
              id="salary"
              aria-describedby="emailHelp"
              placeholder="Salary"
            />
          </div>
          <div className="form-group">
            <label htmlFor="tags" style={{ color: "#007BFF" }}>
              Tags
            </label>
            <Creatable
              isMulti
              onChange={this.handleChange}
              options={alltagvalarr}
              id="tags"
            />
          </div>
          {/* <div className="form-check">
                    <input type="checkbox" className="form-check-input" id="exampleCheck1" />
                    <label className="form-check-label" htmlFor="exampleCheck1">Check me out</label>
                </div> */}
          <button type="submit" className="btn btn-primary">
            Submit
          </button>
        </form>
      </div>
    );
  }
}

export default connect(mapStateToProps, {
  saveJob,
  checkToken,
  getEmployerId,
  getAllTags,
})(Job);
