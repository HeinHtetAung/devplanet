import React, { Component } from 'react';
import $ from 'jquery';
import { connect } from 'react-redux';
import { saveMessage, getChatUsers, getMessage, updateChatUser, updateAllUser, setReadAll } from '../redux/actions/message_action';
import { getUsers } from '../redux/actions/user_action';
import { socketEmitMessage } from '../redux/actions/realtime_action';
import { checkToken } from '../redux/actions/auth_action';

const htmlval =`class People { 
    constructor(){ 

    } 
    dosth(){ 
        alert('hi') 
    } 
    constructor(){ 
        constructor(){ 
            constructor(){ 
                constructor(){ 
                    constructor(){ 
                        constructor(){ 
                            constructor(){ 
                                constructor(){ 
                                    constructor(){ 
                                        constructor(){ 
                                            constructor(){ 
                                                constructor(){ 
                                                    <html>abc</html><p>23232</p><html>abc</html><p>23232</p><html>abc</html><p>23232</p><html>abc</html><p>23232</p><html>abc</html><p>23232</p><html>abc</html><p>23232</p><html>abc</html><p>23232</p><html>abc</html><p>23232</p><html>abc</html><p>23232</p><html>abc</html><p>23232</p><html>abc</html><p>23232</p><html>abc</html><p>23232</p><html>abc</html><p>23232</p><html>abc</html><p>23232</p><html>abc</html><p>23232</p><html>abc</html><p>23232</p><html>abc</html><p>23232</p><html>abc</html><p>23232</p><html>abc</html>
                                                } 
                                            } 
                                        } 
                                    } 
                                } 
                            } 
                        } 
                    } 
                } 
            } 
        } 
    } 
}`;

const mapStateToProps = state => {
	return{
        authinfo : state.auth_reducer.authinfo,
        chat_users : state.message_reducer.chat_users,
        messages : state.message_reducer.messages,
        users : state.user_reducer.users,
        socket : state.realtime_reducer.socket,
        incoming_msg : state.realtime_reducer.incoming_msg
	}
}

class Message extends Component {
    constructor(props) {
        super(props);
    }
    user_id = this.props.authinfo.user_id;
    other_person_id = '';    
    componentDidMount(){
        // only add if is really need, because token is already check in axioconfig, this need only which page is no data get
        // this.props.checkToken(); // check token need to add to user token is expire or not
        var panelwidth = document.getElementById("messagePanel").offsetWidth;
        panelwidth = panelwidth - 55;
        $('#messagePanel pre').css('width',panelwidth);
        var windowheight = $( window ).height();
        windowheight = windowheight - 270;
        $('#messagePanel .messagesBox').css('height',windowheight);
        this.props.getChatUsers(this.props.authinfo.user_id);
        this.openTopUserChatMessage();                
    }
    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.incoming_msg !== this.props.incoming_msg) {  // if message come
			if(this.props.incoming_msg.sent_user_id == this.other_person_id){
                this.getMessage(this.other_person_id);
            }
            this.props.getChatUsers(this.props.authinfo.user_id);
        }			
        if (prevProps.messages !== this.props.messages) {  // if new message
            this.showScrollDown();
        }
	}
    openTopUserChatMessage(){
        setTimeout(() => {            
            if(this.props.chat_users){
                const chat_users = this.props.chat_users;
                if(chat_users.datas){
                    const users = chat_users.datas;
                    if(users.length){
                        if(users.length > 0){
                            setTimeout(() => {  
                                var inds = $('.indicator_txt');
                                inds[0].click();
                            }, 100);        
                        }
                        return '';    
                    }                
                }
            }
            return this.openTopUserChatMessage()
        }, 200);        
    }
    async sendMessage(e){
        e.preventDefault();
        if(this.other_person_id==""){ return false; }
        const user_id = this.props.authinfo.user_id;
        let text = $('#msg_text').val()
        text = text.replace(/\n\r?/g, '<br />');
        var arr = {
            msgtext : text,
            sent_user_id : user_id,
            receive_user_id : this.other_person_id
        };
        const returndata = await this.props.saveMessage(arr);
        if(returndata.result == false && returndata.errors){
            alert(JSON.stringify(returndata.errors));
        }
        if(returndata.result == true){
            $('#frm_reset').click();
            this.getMessage(this.other_person_id);
            this.props.updateAllUser({ datas : [] });
            this.props.getChatUsers(this.props.authinfo.user_id);
            this.props.socketEmitMessage(this.props.socket, this.other_person_id, arr);             
        }
    }
    showScrollDown(){   // function for auto scroll down         
        var ScrollHeight = $("#messagesBox_id").prop("scrollHeight");
        $('#messagesBox_id').animate({ scrollTop: ScrollHeight }, 10);        
    }    
    getMessage(other_person_id){
        this.other_person_id = other_person_id;
        this.props.getMessage(this.user_id, other_person_id);
        this.props.setReadAll(this.user_id, other_person_id, (data) =>{
            console.log(data);
        });
    }
    showChatUsers(){
        if(this.props.chat_users){
            const chat_users = this.props.chat_users;
            if(chat_users.datas){
                const users = chat_users.datas;
                return users.map((u) => {
                    return (
                        <div className={(this.other_person_id==u.other_person_id)? "indicator ind_active" : "indicator"} key={u.other_person_id}>
                            <a href="#" onClick={(e) => this.getMessage(u.other_person_id)} className="indicator_txt">{u.other_person}</a> <span class="unread_cnt">{(u.unread_cnt>0)? '('+u.unread_cnt+')' : ''}</span>
                        </div>
                    );
                });
            }
        }
    }
    showAllUsers(){
        if(this.props.users){
            const users = this.props.users;
            if(users.datas){
                const us = users.datas;
                return us.map((u) => {
                    if(u.id != this.user_id){
                        return (
                            <div className={(this.other_person_id==u.id)? "indicator ind_active" : "indicator"} key={u.id}>
                                <a href="#" onClick={(e) => this.getMessage(u.id)} className="indicator_txt">{u.username}</a>
                            </div>
                        );
                    }
                });
            }
        }
    }
    showMessages(){
        if(this.props.messages.datas){
            return this.props.messages.datas.map((m) => {
                const other_person_id = (m.sent_user_id == this.user_id)? m.receive_user_id : m.sent_user_id;
                return (
                    <div key={m.id} className={(other_person_id != m.sent_user_id)? "messageBox float-right" : "messageBox float-left"}>
                        <div className={(other_person_id != m.sent_user_id)? "message float-right" : "message float-left"}>
                            <p className={(other_person_id != m.sent_user_id)? "h6 float-right" : "h6 float-left"}>{m.sender.username} ({m.createdAt})</p><br/>
                            <div className="message_txt">                                
                                <div dangerouslySetInnerHTML={{__html:m.msgtext}} />
                            </div>
                        </div>
                    </div>                    
                )
            });
        }
    }
    chatNewMessage(){
        $('#search_user').focus();
        this.props.getUsers();
        this.props.updateChatUser({ datas : [] });
    }
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-3">
                        <ul className="nav nav-tabs">
                            <li className="nav-item">
                                <a className="nav-link active" href="#">Direct Chats</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href="#">Questions</a>
                            </li>
                        </ul>
                        <div className="indicatorBorder">
                            <div className="divnewbtn">
                                <input type="text" id="search_user" className="form-control" placeholder="Search Users" />
                            </div>
                            <div>
                                {this.showChatUsers()}                            
                            </div>
                            <div>
                                {this.showAllUsers()}                            
                            </div>                            
                            <div className="divnewbtn">
                                <input type="button" onClick={() => this.chatNewMessage()} className="btn newmsgbtn" value="See Channels" />
                            </div>
                        </div>
                    </div>
                    <div className="col-md-9" id="messagePanel">
                        <div className="row messagesBox" id="messagesBox_id">
                            {this.showMessages()}

                            {/* <div className="messageBox float-left">
                                <div className="message float-left">
                                    <p className="h6 float-left">Htet Aung (23:12)</p><br/>
                                    <div className="message_txt">
                                        This is the message. This is the message.This is the message.This is the message.This is the message.This is the message.This is the message.This is the message.This is the message.This is the message.This is the message.                                
                                    </div>
                                </div>
                            </div>
                            <div className="messageBox float-left">
                                <div className="message float-left">
                                    <p className="h6 float-left">Htet Aung (23:12)</p><br/>
                                    <div className="message_txt">
                                        This is the message. This is the message. This is the message. This is the message. This is the message.                                
                                    </div>
                                </div>
                            </div>
                            <div className="messageBox float-right">
                                <div className="message float-right">
                                    <p className="h6 float-right">Htet Aung (23:12)</p><br/>
                                    <div className="message_txt">
                                        This is the message. This is the message. This is the message. This is the message. This is the message.                                
                                    </div>
                                </div>
                            </div>
                            <div className="messageBox float-right">
                                <div className="message float-right">
                                    <p className="h6 float-right">Htet Aung (23:12)</p><br/>
                                    <div className="message_txt">
                                    This is the message. This is the message.This is the message.This is the message.This is the message.This is the message.This is the message.This is the message.This is the message.This is the message.This is the message.                                
                                </div>
                                </div>
                            </div>
                            <div className="messageBox float-right">
                                <div className="message float-right">
                                    <p className="h6 float-right">Htet Aung (23:12)</p><br/>
                                    <div className="message_txt">
                                        This is the message. This is the message. 
                                    </div>
                                </div>
                            </div>
                            <div className="messageBox float-left">
                                <div className="message float-left">
                                    <p className="h6 float-left">Htet Aung (23:12)</p><br/>
                                    <div className="message_txt">
                                        This is coding message, you need to do like this
                                        <pre>
                                            {htmlval}
                                        </pre> 
                                    </div>
                                </div>
                            </div>
                            <div className="messageBox float-right">
                                <div className="message float-right">
                                    <p className="h6 float-right">Htet Aung (23:12)</p><br/>
                                    <div className="message_txt">
                                        This is coding message, you need to do like this
                                        <pre>
                                            {htmlval}
                                        </pre> 
                                    </div>
                                </div>
                            </div> */}
                        </div>
                        <hr />
                        <div className="row">
                            <div className="col-md-12">
                                <form onSubmit={(e) => this.sendMessage(e)}>                            
                                    <textarea id="msg_text" className="form-control textareabox"></textarea>                                 
                                    <input type="submit" className="btn btn-info" value="Send" /> &nbsp;
                                    <input type="reset" id="frm_reset" className="btn" value="Reset" />                            
                                </form>
                            </div>
                        </div>
                    </div>
                </div>                
            </div>          
        );
    }
}

export default connect(mapStateToProps, {saveMessage, getChatUsers, getMessage, updateAllUser, getUsers, updateChatUser, socketEmitMessage, checkToken, setReadAll})(Message);

