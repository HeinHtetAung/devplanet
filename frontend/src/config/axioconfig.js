import axios from 'axios';
var qs = require('qs');
const axioApi = axios.create({  
    baseURL: 'http://167.172.72.132:5003/api/',
    timeout: 10000,
    withCredentials: true,
    transformRequest: [(data) => qs.stringify(data)],
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
    transformResponse : (data) => {
        if(data == '{"auth":false,"message":"Failed to authenticate token."}'){
            alert("Your token is expired");
            localStorage.removeItem('userinfo');
            window.location.href = '/';
        }
        return JSON.parse(data);
    }
});

if(localStorage.getItem('userinfo')!=null){
    const userinfo = JSON.parse(localStorage.getItem('userinfo'));
    axioApi.defaults.headers.common['x-access-token'] = userinfo.token;
}

export default axioApi