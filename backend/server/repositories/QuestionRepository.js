const BaseRepo = require('./base/BaseRepo')
const db = require('../config/db.config.js');
const Question = db.question;
const Tag = db.tag;
const TagQue = db.tagrelation;

const mapStateToProps = state => {
    return{
        tag_names : this.props.datas.tag_names
    }
}

class QuestionRepository extends BaseRepo{   // if more than extends 2 clase use HOC
	constructor() {
        super();    
        this.model = Question;    
    }
	async getAllQuestionsByUserid(user_id){
		let retresult = {};
		try{
			const datas = await Question.findAll({ where: { user_id: user_id } })
			retresult = this.successResponse({datas : datas});
		}catch(err){
			retresult = this.failResponse({message : 'something wrong in db save'})
		}
		return retresult;
	}
	async create(question){
		let retresult = {};
		try{
			const res = await Question.create({  
				title: question.title,
				description: question.description,
				user_id: question.user_id
			});
			var que_id = res.dataValues.id;
			let tag_id = [];
			for (var i = 0; i < question.tags.length; i++) {
	            if (question.tags[i]) {
	            	const datas = await Tag.find({ where: { id: question.tags[i]['value'] } });
	            	if(!datas) {
	            		const res = await Tag.create({
							name : question.tags[i]['label']
						});
						tag_id[i] = res.dataValues.id;
	            	} else {
	            		tag_id[i] = datas.id;
	            	}
	            }
	        }
	        for (var j = 0; j < tag_id.length; j++) {
	        	if (tag_id[j]) {
	        		const res = await TagQue.create({
						relation_id : que_id,
						tag_id: tag_id[j],
						tag_for: 'question'
					});
					retresult = this.successResponse();
	        	}
	        }
		}catch(err){
			retresult = this.failResponse();
		}
		return retresult;
	}	
	async delete(id){
		let retresult = {};
		try{
			const res = await Question.destroy({  
				where: {
					id:id
				}
			});
	        const delTagQue = await TagQue.destroy({ where: { relation_id : id, tag_for: 'question' } });
			retresult = this.successResponse();
		}catch(err){
			retresult = this.failResponse();
		}
		return retresult;
	}	
	async getQuestionById(questionid){
		let retresult = {};
		try{
			const datas = await Question.findAll({ where: { id: questionid } })
			const tagQuedatas = await TagQue.findAll({ where: { relation_id: questionid, tag_for: 'question' } });
			let tag_names = [];
			for (var i = 0; i < tagQuedatas.length; i++) {
				const tag_name = await Tag.find({ where: { id : tagQuedatas[i]['tag_id'] } });
				tag_names[i] = tag_name;
			}
			let defaultval;
            let defaultvalarr = Array();
            if(tag_names){
                tag_names.map((d, i) => {
                    var label = d.name;
                    var id = d.id;
                    defaultval = { label: label, value: id }
                    defaultvalarr.push(defaultval);
                });
            }
			retresult = this.successResponse({ datas : datas, tag_names : defaultvalarr });
		}catch(err){
			retresult = this.failResponse({message : 'something wrong in db save'})
		}
		return retresult;
	}

	async update(question){
		let retresult = {};
		try{
			const res = await Question.update({  
				title: question.title,
				description: question.description,	
			},{
				where:{
					id:question.id
				}
			});
			const delTagQue = await TagQue.destroy({  
				where: {
					relation_id: question.id,
					tag_for: 'question'
				}
			});
			let tag_id = [];
			for (var i = 0; i < question.tags.length; i++) {
	            if (question.tags[i]) {
	            	const datas = await Tag.find({ where: { id: question.tags[i]['value'] } });
	            	if(!datas) {
	            		const res = await Tag.create({
							name : question.tags[i]['label']
						});
						tag_id[i] = res.dataValues.id;
	            	} else {
	            		tag_id[i] = datas.id;
	            	}
	            }
	        }
	        for (var j = 0; j < tag_id.length; j++) {
	        	if (tag_id[j]) {
	        		const res = await TagQue.create({
						relation_id : question.id,
						tag_id: tag_id[j],
						tag_for: 'question'
					});
	        	}
	        }
			retresult = this.successResponse();
		}catch(err){
			retresult = this.failResponse();
		}
		return retresult;
	}		
}

module.exports = QuestionRepository;