const BaseRepo = require('./base/BaseRepo')
const db = require('../config/db.config.js');
const Job = db.job;
const Emp = db.employer;
const Tag = db.tag;
const TagQue = db.tagrelation;

const mapStateToProps = state => {
    return{
        tag_names : this.props.datas.tag_names
    }
}

class JobRepository extends BaseRepo{   // if more than extends 2 clase use HOC
	constructor() {
        super();    
        this.model = Job;    
    }
	async getAll(){
		let retresult = {};
		try{
			const datas = await Job.findAll({
				include:[
					{
                        attributes:['company_name'],
                        model:Emp, as:'emp'
                    }
				]
			});
			retresult = this.successResponse({datas : datas})
		}catch(err){
			retresult = this.failResponse({message : 'something wrong in db save'})
		}
		return retresult;
	}

	async create(job){
		console.log(job.employer_id);
		let retresult = {};
		try{
			const res = await Job.create({  
				employer_id: job.employer_id,
				job_title: job.job_title,
				job_description: job.job_description,
				salary: job.salary,
				user_id:job.user_id
			});
			var que_id = res.dataValues.id;
			let tag_id = [];
			for (var i = 0; i < job.tags.length; i++) {
	            if (job.tags[i]) {
	            	const datas = await Tag.find({ where: { id: job.tags[i]['value'] } });
	            	if(!datas) {
	            		const res = await Tag.create({
							name : job.tags[i]['label']
						});
						tag_id[i] = res.dataValues.id;
	            	} else {
	            		tag_id[i] = datas.id;
	            	}
	            }
	        }
	        for (var j = 0; j < tag_id.length; j++) {
	        	if (tag_id[j]) {
	        		const res = await TagQue.create({
						relation_id : que_id,
						tag_id: tag_id[j],
						tag_for: 'job'
					});
					retresult = this.successResponse();
	        	}
	        }
		}catch(err){
			retresult = this.failResponse();
		}
		return retresult;
	}	

	async remove(id){
		console.log(id);
		let retresult = {};
		try{
			const res = await Job.destroy({ where: { id :id } });
			const delTagQue = await TagQue.destroy({ where: { relation_id : id, tag_for: 'job' } });
			retresult = this.successResponse();
		}catch(err){
			retresult = this.failResponse();
		}
		return retresult;
	}	

	async get(id){
		let retresult = {};
		try{
			const datas = await Job.findOne({ where: { id :id } });
			const tagQuedatas = await TagQue.findAll({ where: { relation_id: id, tag_for: 'job' } });
			let tag_names = [];
			for (var i = 0; i < tagQuedatas.length; i++) {
				const tag_name = await Tag.find({ where: { id : tagQuedatas[i]['tag_id'] } });
				tag_names[i] = tag_name;
			}
			let defaultval;
            let defaultvalarr = Array();
            if(tag_names){
                tag_names.map((d, i) => {
                    var label = d.name;
                    var id = d.id;
                    defaultval = { label: label, value: id }
                    defaultvalarr.push(defaultval);
                });
            }
			retresult = this.successResponse({datas : datas,tag_names : defaultvalarr})
		}catch(err){
			retresult = this.failResponse({message : 'something wrong in db save'})
		}
		return retresult;
	}
	async update(job){
		let retresult = {};
		try{
			const res = await Job.update({  
				employer_id: job.employer_id,
				job_title: job.job_title,
				job_description: job.job_description,
				salary: job.salary
			},{
				where:{
					id:job.id
				}
			});
			const delTagQue = await TagQue.destroy({  
				where: {
					relation_id: job.id,
					tag_for: 'job'
				}
			});
			let tag_id = [];
			for (var i = 0; i < job.tags.length; i++) {
	            if (job.tags[i]) {
	            	const datas = await Tag.find({ where: { id: job.tags[i]['value'] } });
	            	if(!datas) {
	            		const res = await Tag.create({
							name : job.tags[i]['label']
						});
						tag_id[i] = res.dataValues.id;
	            	} else {
	            		tag_id[i] = datas.id;
	            	}
	            }
	        }
	        for (var j = 0; j < tag_id.length; j++) {
	        	if (tag_id[j]) {
	        		const res = await TagQue.create({
						relation_id : job.id,
						tag_id: tag_id[j],
						tag_for: 'job'
					});
	        	}
	        }
			retresult = this.successResponse();
		}catch(err){
			retresult = this.failResponse();
		}
		return retresult;
	}		

	async getEmpId(user_id){
		let retresult = {};
		try{
			const datas = await Emp.findOne({ where: { user_id :user_id } });
			retresult = this.successResponse({datas : datas})
			console.log(retresult);
		}catch(err){
			retresult = this.failResponse({message : 'something wrong in db save'})
		}
		return retresult;
	}

	async getUserJob(user_id){
		let retresult = {};
		try{
			const datas = await Job.findAll({ 
							where: { user_id :user_id },
							include:[
									{
				                        attributes:['company_name'],
				                        model:Emp, as:'emp'
				                    }
								]
				 			});
			retresult = this.successResponse({datas : datas})
		}catch(err){
			retresult = this.failResponse({message : 'something wrong in db save'})
		}
		return retresult;
	}
}

module.exports = JobRepository;