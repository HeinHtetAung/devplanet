const BaseRepo = require('./base/BaseRepo')
const db = require('../config/db.config.js');
const Developer = db.developer;

class DeveloperRepository extends BaseRepo{
	constructor() {
        super();    
        this.model = Developer;    
    }
}

module.exports = DeveloperRepository;