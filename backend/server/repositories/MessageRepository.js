const BaseRepo = require('./base/BaseRepo')
const db = require('../config/db.config.js');
const Message = db.message;
const User = db.user;
const Op = db.Sequelize.Op;

class MessageRepository extends BaseRepo{
	constructor() {
        super();    
        this.model = Message;    
    }

    /*
     * get user who chat with user_id to show in left panel
     */
    async getChatUsers(user_id){
		let retresult = {};
		try{
            const datas = await this.model.findAll({
                attributes: [
                    [db.Sequelize.literal('(CASE WHEN sent_user_id = '+user_id+' THEN receive_user_id ELSE sent_user_id END)'), 'other_person_id'],
                    [db.Sequelize.literal('(CASE WHEN sent_user_id = '+user_id+' THEN receiver.username ELSE sender.username END)'), 'other_person'],
                    [db.Sequelize.literal('MAX(dp_message.id)'), 'latest_msg'],    
                    [db.Sequelize.literal('COUNT(CASE WHEN `read`=0 and receive_user_id='+user_id+' THEN dp_message.id END)'), 'unread_cnt']            
                ],
                include: [{
                    attributes:['id', 'username', 'email'], 
                    model: User,
                    as: 'sender'
                },{
                    attributes:['id', 'username', 'email'], 
                    model: User,
                    as: 'receiver'
                }],
                where: {
                    // where (sent_user_id = user_id OR receive_user_id = user_id)
                    [Op.or]: [{ 'sent_user_id' : user_id}, {'receive_user_id': user_id}]
                },
                order : [
                    [db.Sequelize.literal('latest_msg'), 'desc']
                ],
                group : ['other_person_id']
            });
			retresult = this.successResponse({datas : datas})
		}catch(err){
            console.log(err);
			retresult = this.failResponse({message : 'something wrong in db get'})
		}
		return retresult;
    }
    
    /*
     * get messages between two user user1 and user2 to show in message panel when click user from left panel
     */
    async getMessages(user1, user2){
		let retresult = {};
		try{
            const datas = await this.model.findAll({
                where: {
                    // WHERE ((sent_user_id = '2' AND receive_user_id = '1') OR (sent_user_id = '1' AND receive_user_id = '2'))
                    [Op.or]: [{ 'sent_user_id' : user1, 'receive_user_id': user2}, {'sent_user_id': user2, 'receive_user_id': user1}]
                },
                include: [{
                    attributes:['id', 'username', 'email'], 
                    model: User,
                    as: 'sender'
                },{
                    attributes:['id', 'username', 'email'], 
                    model: User,
                    as: 'receiver'
                }],
                order : [
                    ['id', 'asc']
                ]
            });
			retresult = this.successResponse({datas : datas})
		}catch(err){
            console.log(err);
			retresult = this.failResponse({message : 'something wrong in db get'})
		}
		return retresult;
    }
    
    async readAll(arr){
        console.log(arr);
		let retresult = {};
		try{
			await this.model.update(
				{ read : 1 },
				{ where: { sent_user_id: arr.sent_user_id, receive_user_id : arr.receive_user_id } }
			);
			retresult = this.successResponse();
		}catch(err){
			console.log(err);
			retresult = this.failResponse();
		}
		return retresult;
	}
}

module.exports = MessageRepository;