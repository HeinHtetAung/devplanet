const BaseRepo = require('./base/BaseRepo')
const db = require('../config/db.config.js');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const env = require('./../config/env');
const User = db.user;
const Employer = db.employer;
const Op = db.Sequelize.Op;
const Developer = require('./DeveloperRepository');

class UserRepository extends BaseRepo{   // if more than extends 2 clase use HOC
	constructor() {
        super();    
		this.model = User;    
		this.developermodel = new Developer();
		this.employermodel = new Employer();
	}
	
	/*
	 * Api for get all users
	 * http://localhost:5000/api/users?page=1&limit=3&orderby=id&order=desc&username=?&id=?
	 */
	async getAll(data){
		var page = (data.page)? data.page : 1;
		page = parseInt(page);
		var limit = (data.limit)? data.limit : 10;
		limit = parseInt(limit);
		var offset = (page-1) * limit;
		var orderby = (data.orderby)? data.orderby : 'id';
		orderby = (orderby != 'id' && orderby != 'username')? 'id' : orderby;
		var order = (data.order)? data.order : 'desc';
		order = (order != 'desc' && order != 'asc')? 'desc' : order;
		let retresult = {};
		
		var condition = {
			offset : offset,
			limit : limit,
			order : [
				[orderby, order]
			]
		};
		
		var where = {};
		var username = (data.username)? data.username : '';
		if(username != ''){
			where.username = { [Op.like]: [`%${username}%`] };  // this is for like			
		}

		var id = (data.id)? data.id : '';
		if(id != ''){
			where.id = id;
		}

		condition.where = where;
		
		console.log(condition);		
		try{
			const datas = await User.findAll(condition);
			// Static way is like this
			// const datas = await User.findAll({ 	
			// 	offset: 0,
			// 	limit: 3,
			// 	order: [ [ 'id', 'asc' ] ],
			// 	where: { 
			// 		username: { [Op.like]: [`%${username}%`] },
			// 		id: '2' 
			// 	}				
			// });
			retresult = this.successResponse({datas : datas})
		}catch(err){
			retresult = this.failResponse({message : 'something wrong in db get'})
		}
		return retresult;
	}
	async create(user){
		console.log(user);
		let retresult = {};
		try{
			const res = await User.create({  
				username: user.username,
				email: user.email,
				password: user.password
			});
			var user_id = res.dataValues.id;
			user.user_id = user_id;
			if(user.is_developer && user.is_developer == true){				
				const data = await this.developermodel.save(user);
				// console.log(data);
			}
			if(user.is_employer && user.is_employer == true){

				const res = await Employer.create({  
					person_name: user.person_name,
					company_name: user.company_name,
					company_info: user.company_info,
					user_id: user.user_id

				});				
			}
			retresult = this.successResponse();
		}catch(err){
			retresult = this.failResponse();
		}
		return retresult;
	}	
	async getUserByEmail(email){
		let retresult = {};
		try{
			const data = await User.findOne({ where: { email: email } })
			if(data != null){
				retresult = this.successResponse({data : data});
			}else{
				retresult = this.failResponse({message : 'user not exist with this email'})
			}
		}catch(err){
			retresult = this.failResponse({message : 'something wrong in db get'})
		}
		return retresult;
	}	

	/*
	 * Login attempt method written in promise structure
	 * @return promise
	 */
	loginAttempt(email, password){
		return new Promise(async (resolve, reject) => {
			const user = await this.getUserByEmail(email);
			if(user.result == true){
				var passwordIsValid = bcrypt.compareSync(password, user.data.password);
				if(passwordIsValid == true){
					// If password valid jwt need to produce token with our secret
					var token = jwt.sign({ id: user.data.id }, env.secret, {
						expiresIn: 86400 // seconds #expires in 24 hours
					});
					resolve(this.successResponse({message:'Login Success', token : token, user_id : user.data.id, username : user.data.username}));
				}					
				reject(this.failResponse({message:'Password incorrect!'}));
			}
			reject(this.failResponse({message:'User not exist'}));
		});
	}
}

module.exports = UserRepository;