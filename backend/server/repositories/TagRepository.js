const BaseRepo = require('./base/BaseRepo')
const db = require('../config/db.config.js');
const Tag = db.tag;

class TagRepository extends BaseRepo{   // if more than extends 2 clase use HOC
	constructor() {
        super();    
        this.model = Tag; 
    }
	async getAllTags(user_id){
		let retresult = {};
		try{
			const datas = await Tag.findAll();
			retresult = this.successResponse({datas : datas});
		}catch(err){
			retresult = this.failResponse({message : 'something wrong in db save'})
		}
		return retresult;
	}
}

module.exports = TagRepository;