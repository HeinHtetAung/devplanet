const BaseRepo = require('./base/BaseRepo')
const db = require('../config/db.config.js');
const Employer = db.employer;

class EmployerRepository extends BaseRepo{
	constructor() {
        super();    
        this.model = Employer;    
    }
}

module.exports = EmployerRepository;