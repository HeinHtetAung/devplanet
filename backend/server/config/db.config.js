const env = require('./env.js');
 
const Sequelize = require('sequelize');
const sequelize = new Sequelize(env.database, env.username, env.password, {
  host: env.host,
  port: env.port,
  dialect: env.dialect,
  operatorsAliases: false,
 
  pool: {
    max: env.max,
    min: env.pool.min,
    acquire: env.pool.acquire,
    idle: env.pool.idle
  }
});
 
const db = {};
 
db.Sequelize = Sequelize;
db.sequelize = sequelize;
 
//Models/tables
db.user = require('../models/user.js')(sequelize, Sequelize);
db.message = require('../models/message.js')(sequelize, Sequelize);
db.developer = require('../models/developer.js')(sequelize, Sequelize);
db.tag = require('../models/tag.js')(sequelize, Sequelize);
db.tagrelation = require('../models/tag_relation.js')(sequelize, Sequelize);
db.question = require('../models/question.js')(sequelize, Sequelize);
db.job = require('../models/job.js')(sequelize, Sequelize);
db.employer = require('../models/employer.js')(sequelize, Sequelize);
db.message.belongsTo(db.user, {foreignKey: 'sent_user_id', as: 'sender'});
db.message.belongsTo(db.user, {foreignKey: 'receive_user_id', as: 'receiver'})
db.developer.belongsTo(db.user, {foreignKey: 'user_id', as: 'user'});
db.question.belongsTo(db.user, {foreignKey: 'user_id', as: 'user'});
db.job.belongsTo(db.user, {foreignKey: 'user_id', as: 'user'});
db.job.belongsTo(db.employer, {foreignKey: 'employer_id', as: 'emp'});
db.employer.belongsTo(db.user, {foreignKey: 'user_id', as: 'user'});

// db.newModel = require('../models/newModel.js')(sequelize, Sequelize); // declare new model

module.exports = db;