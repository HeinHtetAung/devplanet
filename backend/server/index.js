const express = require("express")
const mongoose = require('mongoose')
const cors = require('cors')
const bodyParser = require('body-parser')

const routes = require('./routes/')

const app = express() 
const router = express.Router()   
const expressValidator = require('express-validator');

router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

// const url = process.env.MONGODB_URI || "mongodb://localhost:27017/mernstack"

/** connect to MongoDB datastore */
// try {
//     mongoose.connect(url, {
//         //useMongoClient: true
//         useNewUrlParser: true  // solve error DeprecationWarning: current URL string parser is deprecated, and will be removed in a future version. To use the new parser, pass option { useNewUrlParser: true } to MongoClient.connect.
//     })    
// } catch (error) {
//     console.log(error)
// }

const db = require('./config/db.config.js');
  
// force: true will drop the table if it already exists
db.sequelize.sync().then(() => {
  console.log('Drop and Resync with { force: true }');
});

let port = 5000 || process.env.PORT   // this is our server port, now can run as localhost:5000

routes(router) 	// we can use route by like this

// later do
const corsOptions = {
	origin: ['http://localhost:3000', 'https://mern-stack-course-react.herokuapp.com', 'http://127.0.0.1:8080', 'http://167.172.72.132:5002'],
	credentials:true,
}
app.use(cors(corsOptions))

app.use(bodyParser.json())

app.use(bodyParser.urlencoded({extended : true}))

app.use(expressValidator());

app.use('/api', router)  // this is declare of routes by premix api...

/** start server */
app.set('port', (process.env.PORT || 5003));


app.listen(app.get('port'), ()=>{
	console.log('Node app is running on port', app.get('port'));
})



/*************** Socket connect ****************/
var server = require('http').Server(app);
server.listen(5004);
var io = require('socket.io')(server);
const connections =[];

io.on('connection', function (socket) {
    console.log(socket.id);
    connections.push(socket);

    console.log('Connected : %s sockets conected', connections.length);
    
    socket.on('createConnection', () =>
    {
        socket.emit('connectionResponse',socket.id);
    });

    socket.on('message_send', (data)=>{
        console.log('message_send');
        console.log(data);
        io.emit('message_receive', data);
    });
    
    socket.on('message_read_send', (data)=>{
        console.log('message_read_send');
        console.log(data);
        io.emit('message_read_receive', data);
    });

});
/************* **************** **************/