module.exports = (sequelize, Sequelize) => {
	const Question = sequelize.define('dp_question', {
        title: {
		    type: Sequelize.STRING
	    },
        description: {
            type: Sequelize.STRING
        },
        user_id: {
            type: Sequelize.INTEGER
        }
	});
	return Question;
}