module.exports = (sequelize, Sequelize) => {
	const TagRelation = sequelize.define('dp_tag_relation', {
        tag_id: {
		    type: Sequelize.INTEGER
	    },
	    relation_id: {
		    type: Sequelize.INTEGER
	    },
	    tag_for: {
	    	type: Sequelize.STRING
	    }
	});
	return TagRelation;
}