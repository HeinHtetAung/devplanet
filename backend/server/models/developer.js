module.exports = (sequelize, Sequelize) => {
	const Developer = sequelize.define('dp_developer', {
      current_position: {
        type: Sequelize.STRING  // later change to foreign key from skill tables
      },
	    skill: {
		    type: Sequelize.STRING  // later change to foreign key from skill tables
	    },
	    open_close: {
        type: Sequelize.INTEGER,
        defaultValue : 1,
        validate: {
          isIn: [[0, 1]]
        }
      },
      available_time:{
        type: Sequelize.STRING 
      },
      points: {
        type: Sequelize.INTEGER,
        defaultValue : 0
      },
      user_id : {
        type: Sequelize.INTEGER
      }
	});
	
	return Developer;
}