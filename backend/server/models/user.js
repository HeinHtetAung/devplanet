module.exports = (sequelize, Sequelize) => {
	const User = sequelize.define('dp_user', {
	  username: {
		type: Sequelize.STRING
	  },
	  email: {
	  	type: Sequelize.STRING
	  },
	  password: {
	  	type: Sequelize.STRING
	  }
	});
	
	return User;
}