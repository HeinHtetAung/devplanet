module.exports = (sequelize, Sequelize) => {
	const Job = sequelize.define('db_job', {
	  employer_id: {
		type: Sequelize.INTEGER
	  },
	  job_title: {
		type: Sequelize.STRING
	  },
	  job_description: {
	  	type: Sequelize.STRING
	  },
	  salary: {
	  	type: Sequelize.INTEGER
	  },
	  user_id: {
	  	type: Sequelize.INTEGER
	  }
	});
	
	return Job;
}
