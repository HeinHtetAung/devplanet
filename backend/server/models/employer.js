module.exports = (sequelize, Sequelize) => {
	const Employer = sequelize.define('db_employer', {
	  person_name: {
		type: Sequelize.STRING
	  },
	  company_name: {
	  	type: Sequelize.STRING
	  },
	  company_info: {
	  	type: Sequelize.STRING
	  },
	  user_id: {
	  	type: Sequelize.INTEGER
	  }
	});
	
	return Employer;
}