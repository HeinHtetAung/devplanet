module.exports = (sequelize, Sequelize) => {
	const Tag = sequelize.define('dp_tags', {
        name: {
		    type: Sequelize.TEXT
	    }
	});
	return Tag;
}