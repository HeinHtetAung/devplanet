const QuestionRepository = require('./../repositories/QuestionRepository')
const questionrepo = new QuestionRepository();
const db = require('../config/db.config.js');
const Question = db.question;

module.exports = {    

    /*
     * alternative way really useful and effective https://booker.codes/input-validation-in-express-with-express-validator/
     * rules can check in check.d.ts
     */
    validateSaveQuestion : (req, res, next) => {
        req.checkBody('data.title', 'required').exists().not().isEmpty(); 
        req.checkBody('data.description', 'required').exists().not().isEmpty();         
        req.checkBody('data.tags', 'required').exists().not().isEmpty();         

        req.asyncValidationErrors().then(() => {
            next();
        }).catch((errors) => {
            return res.send({result : false, validateerr : errors});                        
        });
    }
}