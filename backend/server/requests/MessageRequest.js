module.exports = {    
    validateSave : (req, res, next) => {
        req.checkBody('data.sent_user_id', 'required').exists().not().isEmpty(); 
        req.checkBody('data.receive_user_id', 'required').exists().not().isEmpty(); 
        req.checkBody('data.msgtext', 'required').exists().not().isEmpty(); 
        
        // if use async in above
        // req.asyncValidationErrors().then(() => {
        //     next();
        // }).catch((errors) => {
        //     return res.send({result : false, validateerr : errors});                        
        // });
        
        // if it is not db touch, it simple like following
        var errors = req.validationErrors();
        if (errors) {
            return res.send({ result: false, errors : errors });            
        }
        next();
    },
    validateUpdate : (req, res, next) => {
        next();
    }
}

