const JobRepository = require('./../repositories/JobRepository')
const bcrypt = require('bcryptjs');

"use strict";
class JobController {
	constructor(){
		this.jobrepo = new JobRepository();    
	}	

	async getAllJobs(req, res, next){
  		const jobs = await this.jobrepo.getAll();
  		res.send(jobs);
	}

	// getJob(req, res, next){
	// 	const userid = req.params.id;
	// 	res.send({id : userid});
	// }

	async saveJob(req, res, next){
		// console.log("res " + res);
		const savejob = req.body.data;	
		const result = await this.jobrepo.create(savejob);	
		res.send(result);
	}

	async deleteJob(req, res, next){
		const jobid = req.params.id;
		// console.log("jobid");
		// console.log("res " + res);
		const result = await this.jobrepo.remove(jobid);	
		res.send(result);
	}

	async editJob(req, res, next){
		const jobid = req.params.id;
		// console.log("jobid");
		console.log("res " + jobid);
		const result = await this.jobrepo.get(jobid);	
		res.send(result);
	}

	async Updatejob(req, res, next){
		const updatejob = req.body.data;	
		const result = await this.jobrepo.update(updatejob);	
		return res.send(result);
	}

	async getEmpId(req, res, next){
		const user_id = req.params.id;
		console.log("getEmpId");
		console.log("res " + user_id);
		const result = await this.jobrepo.getEmpId(user_id);	
		res.send(result);
	}

	async getUserJob(req, res, next){
		const user_id = req.params.id;
		console.log("getUserJob");
		console.log("res " + user_id);
		const result = await this.jobrepo.getUserJob(user_id);
		res.send(result);
	}
}

module.exports = JobController;