const QuestionRepository = require('./../repositories/QuestionRepository')
const bcrypt = require('bcryptjs');

"use strict";
class QuestionController {
	constructor(){
		this.questionrepo = new QuestionRepository();    
	}	

	async getAllQuestionsByUserid(req, res, next){
		const user_id = req.params.user_id;
		console.log(req.params);
  		const questions = await this.questionrepo.getAllQuestionsByUserid(user_id);
  		res.send(questions);
	}

	async getQuestionById(req, res, next){
		const questionid = req.params.id;
		const question = await this.questionrepo.getQuestionById(questionid);
		res.send(question);
	}

	async saveQuestion(req, res, next){
		const savequestion = req.body.data;	
		const result = await this.questionrepo.create(savequestion);	
		return res.send(result);
	}

	async delQuestions(req, res, next){
		const delid = req.params.id;	
		const result = await this.questionrepo.delete(delid);	
		return res.send(result);
	}
	
	async saveUpdateQuestion(req, res, next){
		const saveupdatequestion = req.body.data;	
		const result = await this.questionrepo.update(saveupdatequestion);	
		return res.send(result);
	}
}

module.exports = QuestionController;