const MessageRepository = require('./../repositories/MessageRepository')
const BaseController = require('./base/BaseController');

"use strict";
class MessageController extends BaseController{
	constructor(){
		super();
		this.repo = new MessageRepository();    
	}

	async getChatUsers(req, res, next){
        const data = await this.repo.getChatUsers(req.params.user_id);
        res.send(data);
        next();
	}
	
	async getMessages(req, res, next){
        const data = await this.repo.getMessages(req.params.user1, req.params.user2);
        res.send(data);
        next();
    }

    async readAll(req, res, next){
    	const data = await this.repo.readAll(req.body.data);
        res.send(data);
        next();
    }
}

module.exports = MessageController;