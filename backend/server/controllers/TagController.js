const TagRepository = require('./../repositories/TagRepository')
const bcrypt = require('bcryptjs');

"use strict";
class TagController {
	constructor(){
		this.tagrepo = new TagRepository();    
	}	

	async getAllTags(req, res, next){
  		const tags = await this.tagrepo.getAllTags();
  		res.send(tags);
	}
}

module.exports = TagController;