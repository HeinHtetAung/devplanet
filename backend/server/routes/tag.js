const TagController = require('./../controllers/TagController') 
const Tagcontroller = new TagController();

module.exports = (router) => {
    /**
     * get all tags
     */
    router.route('/getalltags').get((...args) => {
        Tagcontroller.getAllTags(...args)
    });
}