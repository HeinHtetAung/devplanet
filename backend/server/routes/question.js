// var JwtAuthMiddleware = require("./../Middlewares/JwtAuthMiddleware")
const QuestionController = require('./../controllers/QuestionController') 
const questioncontroller = new QuestionController();
// const { body, check, oneOf, validationResult } = require('express-validator/check');
const QuestionRequest = require('./../requests/QuestionRequest');

module.exports = (router) => {

    /**
     * get a user
     */
    // router
    //     .route('/user/:id')
    //     .get(usercontroller.getUser)

    router.route('/questionsbyuserid/:user_id').get((...args) => {
        questioncontroller.getAllQuestionsByUserid(...args)
    });

    router.route('/questions/save').post(QuestionRequest.validateSaveQuestion,(...args) => {
        questioncontroller.saveQuestion(...args); 
    });

    router.route('/questions/delquestion/:id').get((...args) => {
        questioncontroller.delQuestions(...args)
    });

    router.route('/questions/:id').get((...args) => {
        questioncontroller.getQuestionById(...args)
    });
    
    router.route('/questions/updatequestion/:id').post((...args) => {
        questioncontroller.saveUpdateQuestion(...args)
    });
}