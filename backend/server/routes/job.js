// var JwtAuthMiddleware = require("./../Middlewares/JwtAuthMiddleware")
const JobController = require('./../controllers/JobController') 
const jobcontroller = new JobController();

module.exports = (router) => {

    /**
     * get a job
     */
    // router
    //     .route('/job/:id')
    //     .get(jobcontroller.getjob)

    /**
     * adds a job
     */
    // router
    //     .route('/job')
    //     .post(jobcontroller.addjob)


	// router
 //        .route('/jobs/')
 //        .get(getjob)
    router.route('/jobs').get((...args) => {
        jobcontroller.getAllJobs(...args)
    });

    router.route('/jobs/save').post((...args) => {
        console.log("jobs/save");
        jobcontroller.saveJob(...args)
    });

    router.route('/delete_jobs/:id').get((...args) => {
       console.log("delete_jobs");
       jobcontroller.deleteJob(...args)
    });

    router.route('/jobs/:id').get((...args) => {
       console.log("edit_job");
       jobcontroller.editJob(...args)
    });

    router.route('/updatejob/:id').post((...args) => {
        console.log("update/job");
        jobcontroller.Updatejob(...args)
    });

    router.route('/jobs/emp/:id').get((...args) => {
       console.log("emp_get------------------------------");
       jobcontroller.getEmpId(...args)
    });

    router.route('/jobs/user/:id').get((...args) => {
       console.log("user_job_get");
       jobcontroller.getUserJob(...args)
    });

}