const user = require('./user')
// const post = require('./post')
const auth = require('./auth')
// const tag = require('./tag')
const message = require('./message')

const question = require('./question')

const job = require('./job')
// const newroute = require('./newroute') 

const tag = require('./tag')

module.exports = (router) => {
    user(router)
    
    // post(router)
    auth(router)
    // tag(router)

    message(router)

    question(router)

    tag(router)

    job(router)
}